class CellsPolymer2Trivial extends Polymer.Element {
  static get is() {
    return 'cells-polymer2-trivial';
  }
  static get properties() {
    return {
      pregunta: {
        type: Object,
        value: {
          enunciado: '¿Cual es el mejor framwork de todos?',
          respuetas: ['Angular', 'Polymer', 'React', 'Vue'],
          correcta: 'Polymer'
        }
      },
      respuestaSeleccionada: {
        type: String,
        value: ''
      },
      respuestaCorrecta: {
        type: String,
        value: 'Polymer'
      },
      correcta: {
        type: String,
        value: 'hidden'
      },
      incorrecta: {
        type: String,
        value: 'hidden'
      },
      contestada: {
        type: String,
        value: ''
      }
    };
  }

  // funcion que guarda la respeusta que ha pulsado el usuario
  modificaRespuestaSelecionada(ev) {
    this.respuestaSeleccionada = ev.model.respueta;
    console.log(this.respuestaSeleccionada);
  }

  // funcion que oculta la pregunta y muestra segun toca la capa con acertaste o fallaste
  compruebaRespuesta(ev) {
    console.log(this.respuestaSeleccionada);
    console.log(this.pregunta.contestada);
    this.contestada = 'hidden';
    if (this.respuestaSeleccionada === this.pregunta.correcta) {
      this.correcta = '';
    } else {
      this.incorrecta = '';
    }
  }
}
customElements.define(CellsPolymer2Trivial.is, CellsPolymer2Trivial);

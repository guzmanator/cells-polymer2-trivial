# cells-polymer2-trivial

Your component description.

Example:
```html
<cells-polymer2-trivial></cells-polymer2-trivial>
```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --cells-polymer2-trivial  | Mixin applied to :host     | {}  |
| --cells-fontDefault  | Mixin applied to :host font-family    | sans-serif  |
